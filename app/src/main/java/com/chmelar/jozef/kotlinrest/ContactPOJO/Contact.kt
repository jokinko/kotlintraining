package com.chmelar.jozef.kotlinrest.ContactPOJO

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("net.hexar.json2pojo")
class Contact : Serializable {

    @SerializedName("address")
    var address: Address? = null
    @SerializedName("company")
    var company: Company? = null
    @SerializedName("email")
    var email: String? = null
    @SerializedName("id")
    var id: Long? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("phone")
    var phone: String? = null
    @SerializedName("username")
    var username: String? = null
    @SerializedName("website")
    var website: String? = null

    override  fun toString(): String {
        return "$email $name"
    }

}
