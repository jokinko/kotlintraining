package com.chmelar.jozef.kotlinrest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import com.chmelar.jozef.kotlinrest.ContactPOJO.Contact
import kotlinx.android.synthetic.main.activity_user_detail.*
import org.jetbrains.anko.onClick


class UserDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)
        val contact = intent.getSerializableExtra("Contact") as Contact
        toolbar.setNavigationOnClickListener { onBackPressed() }
        with(contact){
            textViewDetailName.text=name
            textViewDetailEmail.text=email
            textViewDetail1.text="${address?.city} , ${address?.street}  ${address?.zipcode} "
            textViewDetail2.text=website
        }


    }
}
