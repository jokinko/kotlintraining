package com.chmelar.jozef.kotlinrest.API

import android.app.Application
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File


/**
 * Created by jozef on 1/24/2017.
 */
class RetrofitHolder(app: Application) {
    val API: IApiRoutes
    val retrofit: Retrofit
    val okHttpClient = OkHttpClient()

    val httpCacheDirectory = File(app.getCacheDir(), "responses")
    val cache = Cache(httpCacheDirectory, 2048)

    val client = OkHttpClient.Builder().cache(cache).addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }).build()
    init {
        retrofit = Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com")
                .addConverterFactory(MoshiConverterFactory.create())
                .client(client)
                .build()
        API = retrofit.create(IApiRoutes::class.java)
    }

}