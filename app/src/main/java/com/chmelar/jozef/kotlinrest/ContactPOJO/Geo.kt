package com.chmelar.jozef.kotlinrest.ContactPOJO

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("net.hexar.json2pojo")
class Geo: Serializable {

    @SerializedName("lat")
    var lat: String? = null
    @SerializedName("lng")
    var lng: String? = null
}
