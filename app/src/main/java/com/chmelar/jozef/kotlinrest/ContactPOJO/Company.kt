package com.chmelar.jozef.kotlinrest.ContactPOJO

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("net.hexar.json2pojo")
class Company: Serializable {

    @SerializedName("bs")
    var bs: String? = null
    @SerializedName("catchPhrase")
    var catchPhrase: String? = null
    @SerializedName("name")
    var name: String? = null

}
