package com.chmelar.jozef.kotlinrest.Main

import android.support.v7.widget.RecyclerView

import android.support.v7.util.DiffUtil
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chmelar.jozef.kotlinrest.ContactPOJO.Contact
import com.chmelar.jozef.kotlinrest.R
import java.util.*

/**
 * Created by jozef on 1/30/2017.
 */
class ContactAdapter(val onDetailClick:(Contact)->Unit) : RecyclerView.Adapter<ContactViewHolder>() {

    var contactList: List<Contact>?= arrayListOf()

    override fun onBindViewHolder(holder: ContactViewHolder?, position: Int) {
        holder?.bind(contactList!![position])
    }

    override fun getItemCount(): Int {
        return contactList!!.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ContactViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.contact_detail,parent,false)
        return ContactViewHolder(view,onDetailClick)
    }

}