package com.chmelar.jozef.kotlinrest.API

import com.chmelar.jozef.kotlinrest.ContactPOJO.Contact
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers

/**
 * Created by jozef on 1/24/2017.
 */
interface IApiRoutes {
    @GET("/users/")
    fun getContacts(@Header("Cache-Control") cacheControl: String): Call<List<Contact>>
}