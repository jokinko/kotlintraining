package com.chmelar.jozef.kotlinrest.Main


import android.support.v7.widget.RecyclerView
import android.view.View
import com.chmelar.jozef.kotlinrest.ContactPOJO.Contact
import kotlinx.android.synthetic.main.activity_user_detail.view.*
import kotlinx.android.synthetic.main.contact_detail.view.*
import org.jetbrains.anko.onClick

/**
 * Created by jozef on 1/30/2017.
 */
class ContactViewHolder(contactDetailView: View, val onDetailClick:(Contact)->Unit) : RecyclerView.ViewHolder(contactDetailView) {

    fun bind(contact: Contact): Unit = with(itemView) {
        textViewName.text = contact.name
        textViewMail.text = contact.email
        contactDetailLayout.onClick { onDetailClick(contact) }
    }


}