package com.chmelar.jozef.kotlinrest.Main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.chmelar.jozef.kotlinrest.ContactPOJO.Contact
import com.chmelar.jozef.kotlinrest.R
import com.chmelar.jozef.kotlinrest.UserDetailActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity(), IMainView {

    private val contactAdapter = ContactAdapter{ onItemInListClicked(it) }
    lateinit var presenter: MainActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainActivityPresenter(this, application);
        contactRecyclerView.layoutManager = LinearLayoutManager(this)
        contactRecyclerView.adapter = contactAdapter
        swiperefresh.setOnRefreshListener { presenter.loadListDataWithoutCaching() }
        presenter.loadListData()
    }

    override fun setListData(contactList: List<Contact>) {
        contactAdapter.contactList = contactList
        contactAdapter.notifyDataSetChanged()
    }

    override fun disableRefreshAnimation() {
        swiperefresh.isRefreshing=false
    }

    override fun onItemInListClicked(clickedContact: Contact) {
        startActivity<UserDetailActivity>("Contact" to clickedContact)
    }

    override fun onActionBarPlusButtonClicked() {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun displayToast(text: String) {
        toast(text)
    }

}
