package com.chmelar.jozef.kotlinrest.Main

import com.chmelar.jozef.kotlinrest.ContactPOJO.Contact


/**
 * Created by jozef on 1/24/2017.
 */
interface IMainView {
    fun onItemInListClicked(clickedContact: Contact)
    fun onActionBarPlusButtonClicked()
    fun displayToast(text:String)
    fun setListData(contactList:List<Contact>)
    fun disableRefreshAnimation()
}