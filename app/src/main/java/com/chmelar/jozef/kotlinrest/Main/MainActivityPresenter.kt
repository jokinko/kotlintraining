package com.chmelar.jozef.kotlinrest.Main

import android.app.Application
import android.util.Log
import com.chmelar.jozef.kotlinrest.ContactPOJO.Contact
import com.chmelar.jozef.kotlinrest.API.RetrofitHolder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jozef on 1/24/2017.
 */
class MainActivityPresenter (val mainView: IMainView, application: Application) : IMainActivityPresenter {

    val retrofit = RetrofitHolder(application)

    override fun loadListDataWithoutCaching() {
        loadList("no-cache")
    }
    override fun loadListData() {
        loadList("cache")
    }

    //  "no-cache" parameter for loading without cache
    private  fun loadList(cache: String) {

        Log.d("MainActivityPresenter","API Request attempt")
        retrofit.API.getContacts(cache).enqueue(
                object : Callback<List<Contact>> {
                    override fun onResponse(call: Call<List<Contact>>?, response: Response<List<Contact>>?) {
                        val contactList = response?.body() as List<Contact>
                        mainView.setListData(contactList)
                        mainView.disableRefreshAnimation()

                    }

                    override fun onFailure(call: Call<List<Contact>>?, t: Throwable?) {
                        mainView.displayToast("error getting data")
                        mainView.disableRefreshAnimation()
                    }
                }
        )
    }

}