package com.chmelar.jozef.kotlinrest.Main

/**
 * Created by jozef on 1/24/2017.
 */
interface IMainActivityPresenter {

    fun loadListData()
    fun loadListDataWithoutCaching()

}