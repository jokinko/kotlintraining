package com.chmelar.jozef.kotlinrest.ContactPOJO

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("net.hexar.json2pojo")
class Address :Serializable {

    @SerializedName("city")
    var city: String? = null
    @SerializedName("geo")
    var geo: Geo? = null
    @SerializedName("street")
    var street: String? = null
    @SerializedName("suite")
    var suite: String? = null
    @SerializedName("zipcode")
    var zipcode: String? = null

}
